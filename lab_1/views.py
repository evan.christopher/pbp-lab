from django.shortcuts import render
from datetime import datetime, date
from .models import Friend
# Views yang akan ngeproses datanya
# Views sebagai penghubung models dan template
# line 6-16 sebagai introduction page
mhs_name = 'Evan Christopher Samosir'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002,5,19)  # TODO Implement this, format (Year, Month, Date)
npm = 2006597065  # TODO Implement this


def index(request): # yang atas dipanggil semua
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request): #nampilin si friend list
    friends = Friend.objects.all().values()  # TODO Implement this
    # objects.all() bakal ngereturn jadi array yang isinya semua data di database
    # kalo kita mengisi nama di website di admin, akan diambil data-data disini
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
