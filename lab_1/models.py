from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here
# Models = yang ngebuat data nya, gimana cara kita ngebuat objek yang bakal disimpen di database

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    dob = models.DateField(max_length=8)
    # TODO Implement missing attributes in Friend model
