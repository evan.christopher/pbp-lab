from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()  # Mengambil model Friend yang sudah kita buat
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
# Create your views here.

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm()
    if request.method == 'POST':
        form = FriendForm(request.POST)
        form.save()
        return HttpResponseRedirect('http://127.0.0.1:8000/lab-3/')

        if form.is_valid():
            form.save()

    context = {'form' : form} #Menambahkan key dan value ke dalam dictionary
    return render(request, 'lab3_form.html', context)
