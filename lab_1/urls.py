from django.urls import path
from .views import index, friend_list
# Akan menjadi penyedia link
urlpatterns = [
    path('', index, name='index'), # lab-1, String kosong? Akan di pass ke sebelahnya
    path('friends', friend_list) # lab-1/friends buat nampilin friend
    # TODO Add friends path using friend_list Views
]
