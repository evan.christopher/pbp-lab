import 'package:flutter/material.dart';
import 'package:lab_6/calling_data.dart';
import 'package:lab_6/main.dart';
import 'package:lab_6/models.dart';

class SearchBar extends StatefulWidget {
  
  const SearchBar({Key? key}) : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBar();
}

class _SearchBar extends State<SearchBar> {
  String tampung = "";
  bool boolean = false;
  final _formKey = GlobalKey<FormState>();
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(0, 206, 209, 1),
          title: Text('Apotek'),
        ),
        body: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              Text("Yuk Cek Apotik di Kotamu!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      height: 5,
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(120, 60, 179, 113))),
              TextFormField(
                onChanged: (value) => {
                        if (value != '')
                          {
                            setState(
                              () {
                                tampung = value;
                              },
                            )
                          }
                      },
                decoration: new InputDecoration(
                  hintText: 'Masukkan nama kotamu disini...',
                  labelText: 'Nama kota',
                  icon: Icon(Icons.assignment),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                // validator: (value) {
                //   if (value == null || value.isEmpty) {
                //     return 'Please enter some text';
                //   }
                //   tampung = value;
                //   return null;
                // },
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton.icon(
                icon: Icon(Icons.search, size: 36),
                label: Text("Carikan dong!"),
                onPressed: () {
                  // if (_formKey.currentState?.validate() ?? false) {
                  setState(() {
                    boolean = true;
                  });
                  // Respond to button press
                // };
                }),
              boolean ? Expanded(
                child: FutureBuilder<List<Apotik>>(
                  future: callingData(
                      tampung), // a previously-obtained Future<String> or null
                  builder: (context, snapshot) {
                    List<Widget> children;
                    if (snapshot.hasData) {
                      final List<Apotik>? apotik = snapshot.data;
                      return ListView.builder(
                          itemCount: apotik?.length,
                          itemBuilder: (context, index) {
                            return Text(
                              apotik![index].daerah + " " + apotik[index].link,
                
                            );
                          },
                        );
                    } else if (snapshot.hasError) {
                      children = <Widget>[
                        const Icon(
                          Icons.error_outline,
                          color: Colors.red,
                          size: 60,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Text('Error: ${snapshot.error}'),
                        )
                      ];
                    } else {
                      children = const <Widget>[
                        SizedBox(
                          width: 60,
                          height: 60,
                          child: CircularProgressIndicator(),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 16),
                          child: Text('Awaiting result...'),
                        )
                      ];
                    }
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: children,
                      ),
                    );
                  },
                ),
              ):Text(''),
            ],
          ),
        ),
        drawer: Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              // ignore: sized_box_for_whitespace
              Container(
                height: 64,
                child: const DrawerHeader(
                  child: Text(
                    'CovidCares',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Color(0xFF24262A),
                  ),
                ),
              ),
              ListTile(
                title: const Text('Home'),
                onTap: () {
                  // Go to Riwayat Jurnal screen
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Forum'),
                onTap: () {
                  // Go to Riwayat Jurnal screen
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('COVID-19'),
                onTap: () {
                  // Go to Riwayat Jurnal screen
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Vaccine'),
                onTap: () {
                  // Go to Riwayat Jurnal screen
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Nearest Pharmacies and Medicines'),
                onTap: () {
                  // Go to Riwayat Jurnal screen
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Jurnal Baru'),
                onTap: () {
                  // Go to Jurnal Baru page
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ));
  }
}
