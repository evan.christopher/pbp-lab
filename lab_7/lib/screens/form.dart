import 'package:flutter/material.dart';
import 'package:lab_7/main.dart';

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(0,206,209, 1),
        title: Text("Registrasi Apotik K 24"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              // TextField(),
              Text("Form Pendaftaran Registrasi Apotik", textAlign: TextAlign.center, style: TextStyle(height: 3, fontSize: 36, fontWeight: FontWeight.bold, color: Color.fromARGB(120,67,162,176))),
              Icon(Icons.arrow_circle_down_rounded, size: 36),
              Padding(
                padding: EdgeInsets.all(20.0),
                child : TextFormField(
                  decoration: new InputDecoration(
                    hintText: "contoh: Angelina Jolie",
                    labelText: "Nama Lengkap",
                    icon: Icon(Icons.people),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Nama tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child : TextFormField(
                  decoration: new InputDecoration(
                  hintText: "contoh: 08xxxxxxxx00",
                  labelText: "Nomor telefon",
                  icon: Icon(Icons.confirmation_number_sharp),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Nomor telefon tidak boleh kosong';
                  }
                  return null;
                },
              ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child : TextFormField(
                  decoration: new InputDecoration(
                  hintText: "Jl. Bondowoso no. 131, Kecamatan XX, Jawa XX,",
                  labelText: "Alamat Rumah",
                  icon: Icon(Icons.house),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Alamat tidak boleh kosong';
                  }
                  return null;
                },
              ),
              ),
              RaisedButton(
                padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
                child: Text(
                  "Submit here!",
                  style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
                ),
                color: Colors.cyan,
                onPressed: () {
                  print("Pesan tersubmit");
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
